package nemipuka;

public class Pair {
	public int j;
	public int k;
	Pair(int p, int r) {
		j=p;
		k=r;
	}
	
	@Override
    public boolean equals(Object o) { 
  
        // If the object is compared with itself then return true   
        if (o == this) { 
            return true; 
        } 
  
        /* Check if o is an instance of Complex or not 
          "null instanceof [type]" also returns false */
        if (!(o instanceof Pair)) { 
            return false; 
        } 
          
        // typecast o to Complex so that we can compare data members  
        Pair c = (Pair) o; 
          
        // Compare the data members and return accordingly  
        return (this.j == c.j && this.k == c.k);
    } 
}
