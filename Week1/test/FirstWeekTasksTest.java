import static org.junit.Assert.*;
import java.math.BigInteger;
import org.junit.Test;

public class FirstWeekTasksTest {

	FirstWeekTasks test = new FirstWeekTasks();

	@Test
	public void testIsOddCaseEven() {
		assertFalse(test.isOdd(4));
		assertFalse(test.isOdd(-6));
	}

	@Test
	public void testIsOddCaseOdd() {
		assertTrue(test.isOdd(5));
		assertTrue(test.isOdd(-1));
	}

	@Test
	public void testIsPrimeCasePrime() {
		assertTrue(test.isPrime(2));
		assertTrue(test.isPrime(13));
	}

	@Test
	public void testIsPrimeCaseNotPrime() {
		assertFalse(test.isPrime(0));
		assertFalse(test.isPrime(1));
		assertFalse(test.isPrime(6));
	}

	@Test
	public void testMin() {
		int myArr[] = { 15, 2, 17, 1, 22 };
		assertEquals(1, test.min(myArr));
		assertNotEquals(15, test.min(myArr));
	}

	@Test
	public void testMinNegativeNum() {
		int myArr[] = { 15, 2, -17, 1, -22 };
		assertEquals(-22, test.min(myArr));
	}

	@Test
	public void testKthmin() {
		int myArr[] = { 15, 2, 17, 1, 22 };
		assertEquals(2, test.kthmin(2, myArr));
		assertNotEquals(22, test.kthmin(3, myArr));
	}

	@Test
	public void testKthminNegativeNumbers() {
		int myArr[] = { 15, -2, -17, 1, 22 };
		assertEquals(-2, test.kthmin(2, myArr));
	}

	@Test
	public void testGetOddOccurrence() {
		int arr1[] = { 1, 1, 2, 5, 6, 5 };
		int arr2[] = { 1, 3, 2, 1, 3, 3, 2 };
		assertEquals(2, test.getOddOccurrence(arr1));
		assertEquals(3, test.getOddOccurrence(arr2));
	}

	@Test
	public void testGetOddOccurrenceNonExisting() {
		int arr3[] = { 1, 1, 2, 5, 2, 5 };
		assertEquals(-1, test.getOddOccurrence(arr3));
	}

	@Test
	public void testGetAverage() {
		int[] arr = { 3, 4, 5, 6, 7, 5 };
		assertEquals(5, test.getAverage(arr));
	}

	@Test
	public void testPowerPositiveNums() {
		assertEquals(4, test.power(2, 2));
		assertEquals(27, test.power(3, 3));
	}

	@Test
	public void testPowerCornerCases() {

		assertEquals(1, test.power(2, 0));
		assertEquals(2, test.power(2, 1));
	}

	@Test
	public void testGetSmallestMultiple() {
		assertEquals(60, test.getSmallestMultiple(6));
		assertEquals(2520, test.getSmallestMultiple(10));
	}

	@Test
	public void testFactorial() {
		assertEquals(1, test.factorial(1));
		assertEquals(720, test.factorial(6));
	}

	@Test
	public void testFactorialWithZero() {
		assertEquals(1, test.factorial(0));
	}

	@Test
	public void testDoubleFac() {
		assertEquals(720, test.doubleFac(3));
	}

	@Test
	public void testKthFactorial() {
		assertEquals(720, test.kthFactorial(2, 3));
	}

	@Test
	public void testFac() {
		assertEquals(new BigInteger("720"), test.fac(new BigInteger("6")));
	}

	@Test
	public void testKthFac() {
		assertEquals(new BigInteger("720"), test.kthFac(new BigInteger("3"), 2));
	}

	@Test
	public void testScalarSum() {
		int[] a = { 2, 3, 4 };
		int[] b = { 3, 4, 5 };
		assertEquals(38, test.scalarSum(a, b));
	}

	@Test
	public void testMaximalScalarSumSorted() {
		int[] a = { 2, 3, 4 };
		int[] b = { 3, 4, 5 };

		assertEquals(38, test.maximalScalarSum(a, b));
	}

	public void testMaximalScalarSumUnsorted() {
		int[] c = { 4, 3, 2 };
		int[] d = { 3, 5, 4 };

		assertEquals(38, test.maximalScalarSum(c, d));
	}

	@Test
	public void testMaximalScalarSum2() {
		int[] c = { 4, 3, 2 };
		int[] d = { 5, 4, 3 };

		assertEquals(38, test.maximalScalarSum2(c, d));
	}

	@Test
	public void testHasMember() {
		int[] a = new int[5];
		int[] b = { 1, 2, 3 };

		assertFalse(test.hasMember(5, a));
		assertTrue(test.hasMember(3, b));
		assertFalse(test.hasMember(7, b));
	}

	@Test
	public void testPositionBetween2() {
		int arr[] = { 2, 3, 4, 2, 7, 9, 2, 4 };
		assertEquals(6, test.positionBetween2(2, arr)); // arr[2] = 4
	}

	@Test
	public void testPositionBetween2MoreDoubles() {
		int arr[] = { 2, 3, 4, 2, 7, 9, 2, 4 };
		assertEquals(7, test.positionBetween2(0, arr)); // arr[0] = 2
	}

	@Test
	public void testPositionBetween2NoDoubles() {
		int arr[] = { 2, 3, 4, 2, 7, 9, 2, 4 };
		assertEquals(1, test.positionBetween2(1, arr)); // arr[1] = 3
	}

	@Test
	public void testMaxSpan() {
		int arr[] = { 2, 3, 4, 2, 7, 9, 2, 4 };
		int arr2[] = { 3, 2, 1, 3 };
		assertEquals(7, test.maxSpan(arr));
		assertEquals(4, test.maxSpan(arr2));
	}

	@Test
	public void testMaxSpanNoDoubles() {
		int arr[] = { 2, 3, 4, 7, 9 };
		assertEquals(1, test.maxSpan(arr));
	}

	@Test
	public void testSum() {
		int arr[] = { 3, 2, 1, 3 };
		assertEquals(6, test.sum(0, 2, arr));
		assertEquals(3, test.sum(0, 0, arr));
	}

	@Test
	public void testEqualSumSides() {
		int a[] = { 3, 0, -1, 2, 1 };
		int b[] = { 2, 1, 2, 3, 1 };
		assertTrue(test.equalSumSides(a));
		assertFalse(test.equalSumSides(b));
	}

	@Test
	public void testEqualSumSidesIfFirstOrLastIsDeleted() {
		int a[] = { 3, 0 };
		int b[] = { 0, 3 };
		int c[] = { 3, 3 };
		assertTrue(test.equalSumSides(a));
		assertTrue(test.equalSumSides(b));
		assertFalse(test.equalSumSides(c));
	}

	@Test
	public void testReverse() {
		String a = "meso";
		assertEquals("osem", test.reverse(a));
	}

	@Test
	public void testReverseIfSentence() {
		String a = "Obicham meso.";
		assertEquals(".osem mahcibO", test.reverse(a));
		assertNotEquals(".osem mahcibo", test.reverse(a)); // case sensitivity
	}

	@Test
	public void testReverseIfEmptyString() {
		String a = "";
		assertEquals("", test.reverse(a));
	}

	@Test
	public void testReverseEveryWord() {
		String sentence = "Pesho e debel.";
		assertEquals("ohseP e .lebed", test.reverseEveryWord(sentence));
		assertNotEquals("ohsep e .lebed", test.reverseEveryWord(sentence));
	}

	@Test
	public void testReverseEveryWordIfEmptyString() {
		String sentence = "";
		assertEquals("", test.reverseEveryWord(sentence));
		assertNotEquals("a", test.reverseEveryWord(sentence));
	}

	@Test
	public void testReverseEveryWordIfOneWord() {
		String sentence = "meso";
		assertEquals("osem", test.reverseEveryWord(sentence));
		assertNotEquals(" osem", test.reverseEveryWord(sentence));
	}

	@Test
	public void testIsPalindromeStringWithOddLength() {
		assertTrue(test.isPalindrome("alabala"));
		assertFalse(test.isPalindrome("alkbala"));
	}

	@Test
	public void testIsPalindromeStringWithEvenLength() {
		assertTrue(test.isPalindrome("alaala"));
		assertFalse(test.isPalindrome("alkala"));
	}

	@Test
	public void testIsPalindromeEmptyString() {
		assertTrue(test.isPalindrome(""));
	}

	@Test
	public void testIsPalindromeLongWithEvenLength() {
		long a = 2552;
		long b = 2553;
		assertTrue(test.isPalindrome(a));
		assertFalse(test.isPalindrome(b));
	}

	@Test
	public void testIsPalindromeLongWithOddLength() {
		long a = 25452;
		long b = 0;
		long c = 25453;
		assertTrue(test.isPalindrome(a));
		assertTrue(test.isPalindrome(b));
		assertFalse(test.isPalindrome(c));
	}

	@Test
	public void testGetLargestPalindrome() {
		assertEquals(666, test.getLargestPalindrome(670));
		assertEquals(666, test.getLargestPalindrome(666));
		assertEquals(1, test.getLargestPalindrome(1));
	}

	@Test
	public void testCopyCharsManyTimes() {
		assertEquals("moremoremore", test.copyChars("more", 3));
	}

	@Test
	public void testCopyCharsZeroTimes() {
		assertEquals("", test.copyChars("more", 0));
	}

	@Test
	public void testCopyCharsEmptyInput() {
		assertEquals("", test.copyChars("", 3));
	}

	@Test
	public void testMentionsWithOneLetter() {
		assertEquals(2, test.mentions("a", "abc s df a"));
		assertEquals(3, test.mentions(" ", "abc s df a"));
	}

	@Test
	public void testMentionsWithWord() {
		assertEquals(2, test.mentions("salam", "Ima li salam. salam ima."));
		assertEquals(2, test.mentions("kote", "kote. kote"));
		assertEquals(4, test.mentions("what", "whattfwahtfwhatawhathwatwhat"));
	}

	@Test
	public void testMentionsWithEmptyText() {
		assertEquals(0, test.mentions("a", ""));
	}

	@Test
	public void testDecodeUrl() {
		assertEquals(" pesho%2:paos/", test.decodeUrl("%20pesho%2%3Apaos%2F"));
	}

	@Test
	public void testSumOfNumbers() {
		assertEquals(333, test.sumOfNumbers("a300be30p-3kae"));
	}

	@Test
	public void testSumOfNumbersNumbersAtStartOrEnd() {
		assertEquals(222, test.sumOfNumbers("200pe10r12"));
	}

	@Test
	public void testSumOfNumbersRegExp() {
		assertEquals(333, test.sumOfNumbersRegExp("a300be30p-3kae"));

	}

	@Test
	public void testSumOfNumbersRegExpNumbersAtStartOrEnd() {
		assertEquals(222, test.sumOfNumbersRegExp("200pe10r12"));
	}

	@Test
	public void testIsAnagram() {
		assertTrue(test.isAnagram("listen", "silent"));
		assertTrue(test.isAnagram("aab", "aba"));
	}

	@Test
	public void testIsAnagramWithEmptyString() {
		assertTrue(test.isAnagram("", ""));
	}

	@Test
	public void testIsAnagramFalse() {
		assertFalse(test.isAnagram("listen", "silent "));
		assertFalse(test.isAnagram("listen", "sillent"));
	}

	@Test
	public void testHasAnagramOf() {
		assertTrue(test.hasAnagramOf("amazing", "gin"));
		assertFalse(test.hasAnagramOf("amazing", "man"));
	}

	@Test
	public void testHasAnagramOfWithEmptyFristString() {
		assertFalse(test.hasAnagramOf("", "niz"));
	}

	@Test
	public void testHasAnagramOfWithEmptySecondString() {
		assertTrue(test.hasAnagramOf("amazing", ""));
	}

	@Test
	public void testHistogram() {
		short[][] matrix = { { 1, 2, 3 }, { 5, 3, 3 }, { 2, 0, 5 } };
		int[] result = new int[256];
		// Arrays.fill(result, 0);
		result[0] = 1;
		result[1] = 1;
		result[2] = 2;
		result[3] = 3;
		result[5] = 2;
		assertArrayEquals(result, test.histogram(matrix));
	}

}

