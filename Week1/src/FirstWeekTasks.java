import java.lang.Math;
import java.util.Arrays;
import java.math.BigInteger;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class FirstWeekTasks {
	boolean isOdd(int n) {
		if (n % 2 != 0)
			return true;
		return false;
	}

	boolean hasDivisors(int n) {
		for (int i = 4; i < Math.sqrt(n); i++) {
			if (n % i == 0)
				return true;
		}
		return false;
	}

	boolean isPrime(int n) {
		if (n <= 1)
			return false;
		if (n <= 3)
			return true;
		if (n % 2 == 0 || n % 3 == 0)
			return false;
		if (!hasDivisors(n))
			return true;
		return false;
	}

	int min(int[] array) {
		int min = array[0];
		for (int i : array) {
			if (i < min)
				min = i;
		}
		return min;
	}

	int kthmin(int k, int[] array) {
		Arrays.sort(array);
		return array[k - 1];
	}

	// find a number that occurs odd times in an array
	int getOddOccurrence(int[] array) {
		Arrays.parallelSort(array);
		int cur = array[0];
		int times = 1;
		for (int i = 1; i < array.length; i++) {
			if (array[i] == cur)
				times++;
			else {
				if (times % 2 != 0)
					return cur;
				else {
					cur = array[i];
					times = 1;
				}
			}
		}
		if (times > 1) {
			if (times % 2 != 0)
				return cur;
		}
		return -1;
	}

	int getAverage(int[] array) {
		int sum = 0;
		for (int i : array) {
			sum += i;
		}
		return sum / array.length;
	}

	// Do in o(logn)
	long power(int x, int n) {
		long temp;
		if (n == 0)
			return 1;
		temp = power(x, n / 2);
		if (n % 2 == 0)
			return temp * temp;
		else
			return temp * temp * x;

	}

	int min2(int[] arr) {
		int min = 1;
		for (int value : arr) {
			if (value != 1) {
				min = value;
				break;
			}
		}
		if (min == 1)
			return 1;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != 1 && arr[i] < min)
				min = arr[i];
		}
		return min;
	}

	long getSmallestMultiple(int n) {
		long nok = 1;
		int[] multiples = new int[n];
		for (int i = 0; i < n; i++) {
			multiples[i] = i + 1;
		}

		int divisor = 2;
		boolean toInc = false;

		while (divisor != 1) {
			for (int i = 0; i < n; i++) {
				if (multiples[i] % divisor == 0) {
					multiples[i] /= divisor;
					toInc = true;
				}
			}
			if (toInc == false)
				divisor = min2(multiples);
			else
				nok *= divisor;
			toInc = false;
		}
		return nok;
	}

	long factorial(long n) {
		if (n == 1 || n == 0)
			return 1;
		return n * factorial(n - 1);

	}

	long doubleFac(int n) {
		return factorial(factorial(n));

	}

	long kthFactorial(int k, int n) {
		long result = n;
		for (int i = 0; i < k; i++) {
			result = factorial(result);
		}
		return result;
	}

	BigInteger fac(BigInteger n) {
		if (n.equals(BigInteger.ZERO) || n.equals(BigInteger.ONE))
			return BigInteger.ONE;
		else
			return n.multiply(fac(n.subtract(BigInteger.ONE)));
	}

	// new BigInteger ("100")
	BigInteger kthFac(BigInteger n, int k) {
		if (k == 0)
			return BigInteger.ONE;
		else if (k == 1)
			return fac(n);
		else
			return fac(kthFac(n, k - 1));
	}

	// (1 2 3).(3 4 5) = (1.3 + 2.4 + 3.5) = 26
	long scalarSum(int[] a, int[] b) {
		if (a.length != b.length)
			return -1;
		long result = 0;
		for (int i = 0; i < a.length; i++) {
			result += a[i] * b[i];
		}
		return result;
	}

	// (1 2 3)(3 4 5) = 26
	// (1 2 3)(5 4 3) = 22
	long maximalScalarSum(int[] a, int[] b) {
		if (a.length != b.length)
			return -1;
		int size = a.length;
		long result = 0;
		for (int k = 1; k <= size; k++) {
			result += kthmin(k, a) * kthmin(k, b);
		}
		return result;
	}

	long maximalScalarSum2(int[] a, int[] b) {
		Arrays.sort(a);
		Arrays.sort(b);
		return scalarSum(a, b);
	}

	boolean hasMember(int n, int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == n)
				return true;
		}
		return false;
	}

	// i - position of the cur element /arr[i]/ (we are looking for its double)
	// returns the distance between the element in position i and
	// the position of the last encounter of the same element
	int positionBetween2(int i, int[] arr) {
		int end = i;
		for (int pos = i + 1; pos < arr.length; pos++) {
			if (arr[pos] == arr[i]) {
				end = pos;
			}
		}
		return end - i + 1;
	}

	int maxSpan(int[] numbers) {
		int size = numbers.length;
		int checkedNums[] = new int[size];
		int countOfChecked = 0;
		int curNum = numbers[0];
		// In checkedNums I put all of the numbers I've already checked
		checkedNums[countOfChecked] = curNum;
		countOfChecked++;
		int maxSpan = positionBetween2(0, numbers);
		for (int i = 1; i < size; i++) {
			if (!hasMember(numbers[i], checkedNums)) {
				checkedNums[countOfChecked] = numbers[i];
				countOfChecked++;
				int curSpan = positionBetween2(i, numbers);
				if (curSpan > maxSpan) {
					maxSpan = curSpan;
				}
			}
		}
		return maxSpan;
	}

	// v arr[] sumata na elementite ot poziciq start do end
	int sum(int start, int end, int[] arr) {
		int sum = 0;
		for (int i = start; i <= end; i++) {
			sum += arr[i];
		}
		return sum;
	}

	boolean equalSumSides(int[] numbers) {
		int size = numbers.length;
		// if I remove i=0
		if (size > 1 && sum(1, size - 1, numbers) == 0) { // [3,0] -> True
			return true;
		}
		for (int i = 1; i < size - 1; i++) {
			if (sum(0, i - 1, numbers) == sum(i + 1, size - 1, numbers))
				return true;
		}
		// if I remove i=size-1
		if (size > 1 && sum(0, size - 2, numbers) == 0) { // [0,3] -> True
			return true;
		}
		return false;
	}

	String reverse(String argument) {
		int size = argument.length();
		char[] original = argument.toCharArray();
		char[] reversed = new char[size];
		for (int i = 0, j = size - 1; i < size; i++, j--) {
			reversed[i] = original[j];
		}
		return new String(reversed);
	}

	String reverseEveryWord(String arg) {
		if (arg == "")
			return "";
		StringBuilder reversed = new StringBuilder();
		String[] words = arg.split(" ", -1); // all words in the sentence
		for (String word : words) {
			reversed.append(reverse(word));
			reversed.append(" ");
		}
		reversed.deleteCharAt(reversed.length() - 1);
		return reversed.toString();

	}

	boolean isPalindrome(String arg) {
		return (arg.equals(reverse(arg)));
	}

	// Is 'n' a palindrome?
	boolean isPalindrome(long arg) {
		return (isPalindrome(String.valueOf(arg)));
	}

	// up to 670 -> 666; up to 235 -> 232
	long getLargestPalindrome(long n) {
		for (long num = n; num > 0; num--) {
			if (isPalindrome(num)) {
				return num;
			}
		}
		return -1;
	}

	String copyChars(String input, int k) {
		StringBuilder result = new StringBuilder();
		while (k != 0) {
			result.append(input);
			k--;
		}
		return result.toString();
	}

	// How many times a 'word' is found in 'text'
	int mentions(String word, String text) {
		int count = 0;
		int fromIndex = 0;
		int start = 0;
		int wordSize = word.length();
		int textSize = text.length();
		while (fromIndex < textSize) {
			start = text.indexOf(word, fromIndex); // returns the index of the first occurrence
													// of the specified substring /word/
			if (start == -1) { // If there are no more mentions of the word
				return count;
			}
			fromIndex = start + wordSize; // next possible start
			count++;
		}
		return count;
	}

	String decodeUrl(String input) {
		input = input.replace("%20", " ");
		input = input.replace("%3A", ":");
		input = input.replace("%3D", "?");
		input = input.replace("%2F", "/");
		return input;
	}

	// a20a30s 1 ay23d = 74
	int sumOfNumbers(String input) {
		StringBuilder num = new StringBuilder();
		int sum = 0;
		int size = input.length();
		for (int i = 0; i < size; i++) {
			char ch = input.charAt(i);
			if (Character.isDigit(ch)) {
				num.append(ch);
			} else if (num.length() > 0) {
				sum += Integer.parseInt(num.toString());
				num.setLength(0);
			}
		}
		if (num.length() > 0) {
			sum += Integer.parseInt(num.toString());
		}
		return sum;
	}

	// [^0-9]+ // [^kakvo da ne], + - pone 1 simvol ne digit
	int sumOfNumbersRegExp(String input) {
		int sum = 0;
		String[] numbers = input.split("[^0-9]+");

		for (String num : numbers) {
			if (!num.contentEquals(""))
				sum += Integer.parseInt(num);
		}
		return sum;
	}

	// listen silent
	boolean isAnagram(String a, String b) {
		char[] first = a.toCharArray();
		Arrays.parallelSort(first);
		char[] second = b.toCharArray();
		Arrays.parallelSort(second);
		// return (new String(first).contentEquals(new String(second)));
		return Arrays.equals(first, second);
	}

	// String A has or doesn't have a substring that is an anagram of String B?
	boolean hasAnagramOf(String big, String small) {
		int bigSize = big.length();
		int smallSize = small.length();
		char[] buffer = new char[smallSize];
		for (int i = 0; i < bigSize - smallSize + 1; i++) {
			for (int j = 0, k = i; j < smallSize; j++, k++) {
				buffer[j] = big.charAt(k);
			}
			if (isAnagram(String.copyValueOf(buffer), small))
				return true;
		}
		return false;
	}

	int[] histogram(short[][] image) {
		int[] result = new int[256];
		int rows = image.length;
		int cols = image[rows - 1].length;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				result[image[i][j]]++;
			}
		}
		return result;
	}

	static void convertToGreyscale(String imgPath) {
		BufferedImage img = null;
		File f = null;

		// read image
		try {
			f = new File(imgPath);
			img = ImageIO.read(f);
		} catch (IOException exc) {
			System.out.println(exc);
		}

		// get image width and height
		int width = img.getWidth();
		int height = img.getHeight();

		// converts to gray scale;
		// (x,y) - coordinates of pixel
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int pixel = img.getRGB(x, y);

				int alpha = (pixel >> 24) & 0xff;
				int red = (pixel >> 16) & 0xff;
				int green = (pixel >> 8) & 0xff;
				int blue = pixel & 0xff;

				// calculate average to turn it gray
				int avg = (red + green + blue) / 3;

				// replace RGB value with avg
				pixel = (alpha << 24) | (avg << 16) | (avg << 8) | avg;

				img.setRGB(x, y, pixel);
			}
		}

		// write image
		try {
			f = new File(imgPath);
			ImageIO.write(img, "jpg", f);
		} catch (IOException exc) {
			System.out.println(exc);
		}
	}

	public static void main(String[] args) {
		convertToGreyscale("D:\\My IT shit\\JAVA\\galaxy.jpg");
	}
}
