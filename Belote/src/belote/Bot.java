package belote;
import java.util.*;

public class Bot extends Player {
	Bot (String myName) {
		name = myName;
		myHand = new Hand();
	}
	
	public int cutDeck() {
		int position = (int)(Math.random() * 29 + 3);
		return position;
	};

	public void receiveCards(ArrayList<Card> cards) {
		myHand.addCards(cards);
	}

	
}
