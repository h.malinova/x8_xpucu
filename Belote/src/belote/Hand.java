package belote;
import java.util.*;

public class Hand {
	private Set<Card> hand;
	Hand () {
		hand = new TreeSet<Card>();
	}

	public boolean hasCard(Card card) {
		return hand.contains(card);
	}

	public void printHand () {
		for (Card card : hand) {
			System.out.println("Rank: " + card.getRank() + "; Suit: " + card.getSuit());
		}
	}

	public void addCards (ArrayList<Card> cards) {
		for (int i=0; i<cards.size(); i++) {
			hand.add(cards.get(i));
		}
	}

	public void throwCard(Card card) {
		hand.remove(card);
	}
	//TODO get vsichki karti
}
