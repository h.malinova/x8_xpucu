package belote;
public enum Declaration {
	
	TIERCE(20), QUARTE(50), QUINTE(100), CARRE_J(200), CARRE_9(150),
	CARRE_A(100), CARRE_K(100), CARRE_Q(100), CARRE_10(100), BELOTE(20);

	private final int points;
	Declaration (int points) {
		this.points = points;
	}
}