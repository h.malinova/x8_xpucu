package belote;
import java.util.Queue;

public class Game {
	Queue<Player> players;
	Dealer dealer;
	PointSystem pointSystem;

	Game(String nameOfPlayer) {
		Bot east = new Bot("East");
		players.offer(east);
		Bot north = new Bot("North");
		players.offer(north);
		Bot west = new Bot("West");
		players.offer(west);
		Bot player = new Bot(nameOfPlayer);
		players.offer(player);
		dealer = new Dealer();
		pointSystem = new PointSystem();
	}

	public Contract choseContract() {
		return Contract.PASS;
	}
	
	public void initiateGame() {
		Contract contract = Contract.PASS;
		do {
			contract = choseContract();			
		} while (contract.equals(Contract.PASS));
		
	}
	

}
