package belote;
import java.util.ArrayList;
import java.util.List;

public abstract class Player {
	protected String name;
	protected boolean team;
	protected Hand myHand;
	protected PlayLogic myLogic;
	protected Contract contract;
	
	public String getName () {
		return name;
	}
	
	public boolean getTeam() {
		return team;
	}
	
	public void receiveCards(List<Card> cards) {};
	
	public int cutDeck () {return 0;};
	
	// vrushta kum deck vsichki karti
	public List<Card> returnCards () {return new ArrayList<Card>();};
	
	// public Card playCard() {};
	
	Contract bid (Contract currentHighest) {return Contract.PASS;};
	
	Declaration announce() {return Declaration.TIERCE;}; 
}
