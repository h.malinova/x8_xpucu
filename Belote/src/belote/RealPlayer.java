package belote;
import java.util.ArrayList;
import java.util.Scanner;

public class RealPlayer extends Player {
	RealPlayer (String myName) {
		name = myName;
		myHand = new Hand();
	}
	public int cutDeck() {
		Scanner in = new Scanner(System.in);
		int position = in.nextInt();
		in.close();
		return position;
	};
	
	public void receiveCards(ArrayList<Card> cards) {};
	
}
