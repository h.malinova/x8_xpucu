package file.system;

import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.junit.Test;

import file.system.File;

public class FileTest {

	// private final ByteArrayOutputStream catchSysOutContent = new ByteArrayOutputStream();
	// private final PrintStream originalOutStream = System.out;

	@Test
	public void test_GetSize() {
		File f = new File("sos.txt", null);
		f.addNewLineToTheEndOfTheContent("Pesho");
		f.addNewLineToTheEndOfTheContent("Obicha Pukanki");
		int size = 2 + f.showLine(1).length() + f.showLine(2).length();
		assertEquals(size, f.getSize());
	}

	@Test
	public void test_ShowLine_When_ExistingLine() {
		File f = new File("sos.txt", null);
		f.addNewLineToTheEndOfTheContent("Rosa");
		assertEquals("Rosa", f.showLine(1));
	}

	@Test
	public void test_ShowLine_When_NonExistingLine() {
		File f = new File("sos.txt", null);
		assertEquals("", f.showLine(1));
		f.addNewLineToTheEndOfTheContent("Something");
		assertEquals("", f.showLine(5));
	}

	@Test
	public void test_ShowContent() {
		File f = new File("sos.txt", null);
		f.addNewLineToTheEndOfTheContent("Pesho");
		f.addNewLineToTheEndOfTheContent("Gosho");
		f.addNewLineAtPosition(5, "Salamcho");
		String expected = "Pesho" + "\n" + "Gosho" + "\n" + "\n" + "\n" + "Salamcho" + "\n";
		assertEquals(expected, f.showContent());
	}

	@Test
	public void test_AddNewLineToTheEndOfTheContent_When_AddOneLine() {
		File f = new File("sos.txt", null);
		f.addNewLineToTheEndOfTheContent("Pesho");
		assertEquals("Pesho", f.showLine(1));
	}

	@Test
	public void test_AddNewLineToTheEndOfTheContent_When_AddMultipleLines() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		String c = "Tosho.";
		f.addNewLineToTheEndOfTheContent(a);
		f.addNewLineToTheEndOfTheContent(b);
		f.addNewLineToTheEndOfTheContent(c);
		assertEquals(a, f.showLine(1));
		assertEquals(b, f.showLine(2));
		assertEquals(c, f.showLine(3));
		assertEquals("", f.showLine(4));
	}

	public void test_AddNewLineAtPosition_When_ValidLine() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		f.addNewLineAtPosition(1, a);
		f.addNewLineAtPosition(3, b);
		assertEquals(a, f.showLine(1));
		assertEquals("", f.showLine(2));
		assertEquals(b, f.showLine(3));
	}

	public void test_AddNewLineAtPosition_When_ExistingLine() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		f.addNewLineAtPosition(1, a);
		f.addNewLineAtPosition(2, b);
		f.addNewLineAtPosition(2, "a");
		assertEquals(a, f.showLine(1));
		assertEquals(b, f.showLine(2));
	}

	@Test
	public void test_ReplaceLineWithNewContentAtPosition_When_ValidLine() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		String c = "Tosho.";
		f.addNewLineToTheEndOfTheContent(a);
		f.addNewLineToTheEndOfTheContent(b);
		f.addNewLineToTheEndOfTheContent(c);

		String d = "Sashko.";
		f.replaceLineWithNewContentAtPosition(3, d);

		assertEquals(a, f.showLine(1));
		assertEquals(b, f.showLine(2));
		assertEquals(d, f.showLine(3));
	}

	@Test
	public void test_ReplaceLineWithNewContentAtPosition_When_NonExistentLine() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		f.addNewLineToTheEndOfTheContent(a);
		f.addNewLineToTheEndOfTheContent(b);

//		System.setOut(new PrintStream(catchSysOutContent));

		String d = "Sashko.";
		f.replaceLineWithNewContentAtPosition(5, d);
		assertEquals(d, f.showLine(5));

//		String error = "Invalid position. You can't replace this line with new content.";
//		assertEquals(error + System.lineSeparator(), catchSysOutContent.toString());
//		catchSysOutContent.close();
//		System.setOut(originalOutStream);
	}

	@Test
	public void test_appendLineToContentAtPosition_When_AppendToValidLine() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		String c = "Sorry, Goshe.";
		f.addNewLineToTheEndOfTheContent(a);
		f.addNewLineToTheEndOfTheContent(b);
		f.appendLineToContentAtPosition(2, c);
		assertEquals(a, f.showLine(1));
		assertEquals(b + c, f.showLine(2));
	}

	@Test
	public void test_appendLineToContentAtPosition_When_NonExistentLine() {
		File f = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		String b = "Gosho ne go haresvame.";
		String c = "Sorry, Goshe.";
		f.addNewLineToTheEndOfTheContent(a);
		f.addNewLineToTheEndOfTheContent(b);

//		System.setOut(new PrintStream(catchSysOutContent));
		f.appendLineToContentAtPosition(5, c);
		assertEquals(c, f.showLine(5));
//		String error = "Invalid position. You can't append this text to this line.";
//		assertEquals(error + System.lineSeparator(), catchSysOutContent.toString());
//		catchSysOutContent.close();
//		System.setOut(originalOutStream);	
	}

	@Test
	public void testEqualsObject_When_Equal() {
		File f1 = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		File f2 = new File("sos.txt", null);
		f1.addNewLineToTheEndOfTheContent(a);
		f2.addNewLineToTheEndOfTheContent(a);
		assertTrue(f1.equals(f2));
	}

	@Test
	public void testEqualsObject_When_NotEqual() {
		File f1 = new File("sos.txt", null);
		String a = "Pesho e lubov.";
		File f2 = new File("sos.txt", null);
		f1.addNewLineToTheEndOfTheContent(a);
		f2.addNewLineToTheEndOfTheContent(a + "b");
		assertFalse(f1.equals(f2));
	}
}
