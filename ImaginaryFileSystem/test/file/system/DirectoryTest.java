package file.system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DirectoryTest {

	private Directory parentDir;
	private Directory childDir;
	private FileStub childFile;

	@Before
	public void setUp() {
		parentDir = new Directory("\\", null);
		childDir = new Directory("home", null);
		childFile = new FileStub("f1.txt", null);
		parentDir.addChild(childDir);
		parentDir.addChild(childFile);
	}

	@Test
	public void test_GetSize() {
		FileStub f2 = new FileStub("f2.txt", null);
		childDir.addChild(f2);
		int expectedSize = f2.getSize() + childFile.getSize();
		assertEquals(expectedSize, parentDir.getSize());
	}

	@Test
	public void test_GetAllChildren_When_MultipleChildren() {
		List<String> expected = new ArrayList<>();
		expected.add("f1.txt");
		expected.add("home");

		List<String> result = parentDir.getAllChildren();
		Collections.sort(result);
		assertEquals(expected, result);
	}

	@Test
	public void test_GetAllChildren_When_NoChildren() {
		List<String> result = childDir.getAllChildren();
		assertTrue(result.isEmpty());
	}

	@Test
	public void test_GetChild_When_ValidChild() {
		assertEquals(childDir, parentDir.getChild("home"));
	}

	@Test
	public void test_GetChild_When_NonExistentChild() {
		assertEquals(null, parentDir.getChild("pesho.txt"));
	}

	@Test
	public void test_GetFile_When_ContainsFile() {
		assertEquals(childFile, parentDir.getFile(childFile.getName()));
	}

	@Test
	public void test_GetFile_When_DoesntContainFile() {
		FileStub notInFileSystem = new FileStub("a.txt", null);
		assertEquals(null, parentDir.getFile(notInFileSystem.getName()));
	}

	@Test(expected = ClassCastException.class)
	public void test_GetFile_When_ContainsDirectory_Should_ThrowException() {
		parentDir.getFile(childDir.getName());
	}

	@Test
	public void test_GetDirectory_When_ContainsDirectory() {
		assertEquals(childDir, parentDir.getDirectory(childDir.getName()));
	}

	@Test
	public void test_GetDirectory_When_DoesntContainDirectory() {
		Directory notInFileSystem = new Directory("a", null);
		assertEquals(null, parentDir.getDirectory(notInFileSystem.getName()));
	}

	@Test(expected = ClassCastException.class)
	public void test_GetDirectory_When_ContainsFile_Should_ThrowException() {
		parentDir.getDirectory(childFile.getName());
	}

	@Test
	public void test_ContainsFile_When_True() {
		assertTrue(parentDir.containsFile(childFile.getName()));
	}

	@Test
	public void test_ContainsFile_When_False() {
		FileStub notInFileSystem = new FileStub("a.txt", null);
		assertFalse(parentDir.containsFile(notInFileSystem.getName()));
	}

	@Test
	public void test_ContainsFile_When_ContainsDirectory() {
		assertFalse(parentDir.containsFile(childDir.getName()));
	}

	@Test
	public void test_ContainsDirectory_When_True() {
		assertTrue(parentDir.containsDirectory(childDir.getName()));
	}

	@Test
	public void test_ContainsDirectory_When_False() {
		Directory notInFileSystem = new Directory("a", null);
		assertFalse(parentDir.containsDirectory(notInFileSystem.getName()));
	}

	@Test
	public void test_ContainsDirectory_When_ContainsFile() {
		assertFalse(parentDir.containsDirectory(childFile.getName()));
	}

	@Test
	public void test_AddChild_When_AddNewFile() {
		FileStub f2 = new FileStub("a.txt", null);
		assertTrue(parentDir.addChild(f2));
		assertTrue(parentDir.containsFile(f2.getName()));
		assertEquals(parentDir, f2.getParent());
	}

	@Test
	public void test_AddChild_When_AddFileWithSameName() {
		FileStub f2 = new FileStub("f1.txt", null);
		assertFalse(parentDir.addChild(f2));
		assertNotEquals(parentDir, f2.getParent());
	}

	@Test
	public void test_AddChild_When_AddNewNonEmptyDirectory() {
		Directory d2 = new Directory("a", null);
		FileStub f2 = new FileStub("a.txt", null);
		d2.addChild(f2);

		assertTrue(parentDir.addChild(d2));
		assertTrue(parentDir.containsDirectory(d2.getName()));
		assertEquals(parentDir, d2.getParent());
	}

	@Test
	public void test_AddChild_When_AddDirectoryWithSameName() {
		Directory d2 = new Directory("home", null);
		assertFalse(parentDir.addChild(d2));
		assertNotEquals(parentDir, d2.getParent());
	}

	@Test
	public void test_ShowChildren() {
		String result = "Type: " + childFile.showType() + "\t" + "Name: " + childFile.getName() + "\n" + "Type: "
				+ childDir.showType() + "\t" + "Name: " + childDir.getName() + "\n";
		assertEquals(result, parentDir.showChildren());
	}

	@Test
	public void test_ShowChildren_When_NoChildren() {
		String expected = "The directory " + childDir.getName() + " is empty.";
		assertEquals(expected, childDir.showChildren());
	}

	@Test
	public void test_EqualsObject_When_True() {
		Directory dir2 = new Directory("\\", null);
		Directory child2 = new Directory("home", null);
		FileStub file2 = new FileStub("f1.txt", null);
		dir2.addChild(child2);
		dir2.addChild(file2);
		assertTrue(parentDir.equals(dir2));
	}

	@Test
	public void test_EqualsObject_When_False() {
		Directory dir2 = new Directory("\\", null);
		Directory child2 = new Directory("home", null);
		FileStub file2 = new FileStub("f2.txt", null);
		dir2.addChild(child2);
		dir2.addChild(file2);
		assertFalse(parentDir.equals(dir2));
	}
}
