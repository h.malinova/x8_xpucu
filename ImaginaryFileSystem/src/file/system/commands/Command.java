package file.system.commands;

import file.system.Directory;

public interface Command {
	// returns "" for success and an error string when something's off
	String execute(String[] input, Directory currentDir); 
	//String showDescription();
}
