package file.system.commands;

import java.util.regex.Pattern;

import file.system.Directory;
import file.system.UserInterface;

public abstract class PathManager {
	protected Directory currentlyIn;
	
	public Directory goTo (String path, UserInterface ui) {
		String fullPath = new PathBuilder().buildPath(path, ui.getCurrentDirectory());
		currentlyIn = ui.getRoot();
		String[] directories = fullPath.split(Pattern.quote("/")); //TODO vij si go
		for (int i =1; i<directories.length; i++) {
			if (currentlyIn.containsDirectory(directories[i])) {
				currentlyIn = currentlyIn.getDirectory(directories[i]);
			}
			else {
				goToNonExistentFolder(); // da hvurli exception ili da suzdade file/dir
			}
		}
		return currentlyIn;
	}
	
	protected String[] splitPath (String[] path) {
		
	}
	
	protected abstract void goToNonExistentFolder();
}
