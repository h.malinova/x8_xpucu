package file.system.commands;

import file.system.Directory;

public class PathBuilder {
	// returns full path
	public String buildPath(String path, Directory currentDir) {
		// relative path or full path
		if (path.charAt(0) == '/') {
			return path;
		} else { // relative
			StringBuilder fullPath = new StringBuilder(path);
			while (currentDir.getParent() != null) {
				fullPath.insert(0, currentDir.getParent().getName() + "/");
				currentDir = currentDir.getParent();
			}
			return fullPath.toString();
		}
	}
}
