package file.system.commands;

import file.system.Directory;
import file.system.UserInterface;

public class Cat implements Command{
	
	// only for files;
	// works for multiple arguments
	
	// TODO return it like a string
	
	@Override
	public String execute(String[] input, Directory currentDir) {
		if (input.length >= 2) {
			for (int i=1;i<input.length; i++) {
				// if a file is valid
				if (currentDir.containsFile(input[i])) {
					System.out.println("Name of file: " + input[i]);
					System.out.println(currentDir.getFile(input[i]).showContent());
					System.out.println("****************************");	
				}
			}
			return "";
		}
		else {
			return "Invalid number of arguments!";
		}
	}

//	@Override
//	public String showDescription() {
//		String description = "Shows content of file.";
//		return description;
//	}
}
