package file.system.commands;

import java.util.Collections;
import java.util.List;

import file.system.Directory;

public class ListContent implements Command {

	@Override
	public String execute(String[] input, Directory currentDir) {
		if (input.length == 1) {
			return ls(input, currentDir);
		}
		else if (input.length == 2 && input[1].equalsIgnoreCase("-sorted")) {
			return lsSorted(input, currentDir);
		}
		else 
			return "Invalid arguments!";
	}
	
	public String ls(String[] input, Directory currentDir) {
		return currentDir.showChildren();
	}
	
	public String lsSorted(String[] input, Directory currentDir) {
		List<String> reversedDesc = currentDir.getAllChildren();
		Collections.sort(reversedDesc, Collections.reverseOrder());
		StringBuilder result = new StringBuilder();
		for (String a : reversedDesc) {
			result.append(a + "\n");
		}
		return result.toString();
	}
}
