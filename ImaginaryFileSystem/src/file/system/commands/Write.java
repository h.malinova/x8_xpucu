//package file.system.commands;
//
//import file.system.Directory;
//import file.system.File;
//
//public class Write implements Command {
//
//	@Override
//	public String execute(String[] input, Directory currentDir) {
//		// write fileName lineNum lineContent
//		if (input.length == 4 && writeAppend(input,currentDir)) {
//			return "";
//		}
//		// write -overwrite fileName lineNum lineContent
//		else if (input.length == 5 && writeOverWrite(input, currentDir)) {
//			return "";
//		} else {
//			return "Invalid arguments!";
//		}
//	}
//
//	// write fileName lineNum lineContent
//	boolean writeAppend(String[] input, Directory currentDir) {
//		String fileName = input[1];
//		if (currentDir.containsFile(fileName)) {
//			File fileToWriteIn = currentDir.getFile(fileName);
//			int lineNumber = Integer.parseInt(input[2]); // TODO exception
//			return fileToWriteIn.appendLineToContentAtPosition(input[3], lineNumber);
//		}
//		return false;
//	}
//
//	// write -overwrite fileName lineNum lineContent
//	boolean writeOverWrite(String[] input, Directory currentDir) {
//		String fileName = input[2];
//		if (input[1].equalsIgnoreCase("-overwrite") && currentDir.containsFile(fileName)) {
//			File fileToWriteIn = currentDir.getFile(fileName);
//			int lineNumber = Integer.parseInt(input[3]); // TODO exception
//			return fileToWriteIn.replaceLineWithNewContentAtPosition(lineNumber, input[4]);
//		} else {
//			return false;
//		}
//	}
//
//}
