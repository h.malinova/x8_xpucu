package file.system.commands;

import file.system.Directory;
import file.system.UserInterface;

// mkdir <dirName>
public class MakeDirectory implements Command {
	
	@Override
	public String execute(String[] input, Directory currentDir) {
		if (input.length == 2) { // correct number of arguments
			String newDirectoryName = input[1];
			
			// if we already have such directory
			if (currentDir.containsDirectory(newDirectoryName)) {
				return "Directory " + newDirectoryName + " already exists.";
			}
			else {
				currentDir.addChild(new Directory(newDirectoryName, currentDir));
				return "";
			}
		}
		else {
			return "Invalid arguments!";
		}
	}		
}
