package file.system.commands;

import file.system.Directory;
import file.system.File;
import file.system.UserInterface;

public class CreateFile implements Command {

	// can create multiple files
	@Override
	public String execute(String[] input, Directory currentDir) {
		if (input.length >= 2) {
			for (int i = 1; i < input.length; i++) {
				File fileToAdd = new File (input[i], currentDir);
				currentDir.addChild(fileToAdd);
			}
			return "";
		} else {
			return "Invalid arguments!";
		}
	}
}
