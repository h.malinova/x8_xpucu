package file.system;

import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

public class File implements SystemFiles {
	private String fileName;
	private Directory fileParent;
	private Map<Integer, String> content; // <lineNumber, lineContent>

	////////////////////////
	public File(String name, Directory parent) {
		fileName = name;
		fileParent = parent;
		content = new HashMap<Integer, String>();
	}

	////////////////////////
	public String getName() {
		return fileName;
	}

	public Directory getParent() {
		return fileParent;
	}

	public void setParent(Directory newParent) {
		fileParent = newParent;
	}

	public int getSize() { // number of lines + number of characters
		int size = 0;
		int lines = Collections.max(content.keySet());
		for (int i = 1; i <= lines; i++) {
			if (content.containsKey(i)) {
				size += content.get(i).length();
			}
		}
		return size + lines;
	}

	public String showType() {
		return "File";
	}

	public String showLine(int lineNumber) {
		if (content.containsKey(lineNumber)) {
			return content.get(lineNumber);
		} else {
			return "";
		}
	}

	public String showContent() {
		if (content.size() > 0) {
			int max = Collections.max(content.keySet());
			StringBuilder result = new StringBuilder();
			for (int i = 1; i <= max; i++) {
				result.append(showLine(i));
				result.append("\n");
			}
			return result.toString();
		} else {
			return "";
		}
	}

	////////////////////////
	public void addNewLineToTheEndOfTheContent(String input) {
		if (content.size() > 0) {
			int max = Collections.max(content.keySet());
			content.put(++max, input);			
		}
		else {
			content.put(1, input);
		}
	}

	public void addNewLineAtPosition(int position, String input) {
		if (! content.containsKey(position)) {
			content.put(position, input);
		}
	}

	public void replaceLineWithNewContentAtPosition(int position, String input) {
		if (showLine(position).equals("")) {
			addNewLineAtPosition(position, input);
		} 
		else {
			content.put(position, input);
		}
	}

	public void appendLineToContentAtPosition(int position, String input) {
		if (content.containsKey(position)) {
			StringBuilder newLine = new StringBuilder();
			newLine.append(content.get(position));
			newLine.append(input);
			content.put(position, newLine.toString());
		} else {
			addNewLineAtPosition(position, input);
		}
	}

	///////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof File)) {
			return false;
		}
		File otherFile = (File) o;
		return (fileName.equals(otherFile.getName()) && content.equals(otherFile.content));
	}
}