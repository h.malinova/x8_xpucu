package file.system;

public class FileStub extends File{
	FileStub(String name, Directory parent) {
		super(name, parent);
	}
	public int getSize() {
		return 10;
	}
}
