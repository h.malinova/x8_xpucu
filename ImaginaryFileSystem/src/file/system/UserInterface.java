package file.system;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import file.system.commands.Command;

public class UserInterface {
	Directory root;
	Directory currentDirectory;
	Map<String, Command> allCommands;
	
	public UserInterface() {
		Directory master = new Directory ("", null); // ime "" ? // TODO
		Directory home = new Directory ("home", null);
		master.addChild(home);
		root = master;
		currentDirectory = home;
		allCommands = new HashMap <String, Command>();
	}
	// TODO fix this, move them to Directory class and fix Commands afterwards
	public Directory getCurrentDirectory() {
		return currentDirectory;
	}
	
	public Directory getRoot() {
		return root;
	}
	
		public void goToParentDirectory() {
		currentDirectory = currentDirectory.getParent();
	}
	
	public boolean goToChildDirectory(Directory child) {
		if (child != null && currentDirectory.containsDirectory(child.getName())) {
			currentDirectory = child;
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean addToCommands(String commandName, Command someCommand) {
		if (allCommands.containsKey(commandName)) {
			System.out.println("Command \"" + commandName + "\" already exists.");
			return false;
		}
		else {
			allCommands.put(commandName, someCommand);
			return true;
		}
	}
	
	public void showAllCommands() {
		for (String commandName : allCommands.keySet()) {
			// TODO
			//System.out.println(commandName + ": " + allCommands.get(commandName).showDescription());
		}
	}
	
	public void init() {
		Scanner input = new Scanner(System.in);
		String command = null;
		do {
			System.out.println("Enter command: ");
			command = input.nextLine();
			executeCommand(command);
		} while (command.equalsIgnoreCase("exit"));
		// TODO help
		input.close();
	}
	
	public void executeCommand (String wantedCommand) {
		String[] command = wantedCommand.split(" ");
		try {
			allCommands.get(command[0]).execute(command, currentDirectory);
		}
		catch (Exception e) {
			System.out.println("No such command found. Try again!");
		}
	}
	
}
