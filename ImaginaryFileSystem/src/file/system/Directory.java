package file.system;

import java.util.stream.Collectors;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

public class Directory implements SystemFiles {
	private String dirName;
	private Directory dirParent;
	private Map<String, SystemFiles> children; // key(name) - value (file/dir)

	public Directory(String name, Directory parent) {
		dirName = name;
		dirParent = parent;
		children = new HashMap<String, SystemFiles>();
	}

	///////////////////
	public String getName() {
		return dirName;
	}

	public Directory getParent() {
		return dirParent;
	}
	
	public void setParent(Directory newParent) {
		dirParent = newParent;
	}
	
	public int getSize() {
		int size = 0;
		for (SystemFiles s : children.values()) {
			size += s.getSize();
		}
		return size;
	}
	
	public String showType() {
		return "Directory";
	}

	// TODO koe mi trqbva realno?
	public List<String> getAllChildren() {
		return children.keySet().stream().collect(Collectors.toList());
	}
	
	public SystemFiles getChild (String name) {
		return children.get(name);
	}
	
	public File getFile(String file) {
		return (File) children.get(file);
	}

	public Directory getDirectory(String dir) {
		return (Directory) children.get(dir);
	}
	
	public boolean containsFile(String name) {
		if (children.get(name) != null) {
			try {
				if (children.get(name) instanceof File) {
					return true;
				}
			} catch (Exception e) {
				System.out.println(name + " is not a " + showType());
			}
		}
		return false;
	}

	public boolean containsDirectory(String name) {
		if (children.get(name) != null) {
			try {
				if (children.get(name) instanceof Directory) {
					return true;
				}
			} catch (Exception e) {
				System.out.println(name + " is not a " + showType());
			}
		}
		return false;
	}

	///////////////////
	public boolean addChild(SystemFiles child) {
		if (!(containsDirectory(child.getName()) || containsFile(child.getName()))) {
			children.put(child.getName(), child);
			child.setParent(this);
			return true;
		} else {
			return false;
		}
	}

	///////////////////
	public String showChildren() {
		StringBuilder result = new StringBuilder();
		children.forEach((key, value) -> result.append("Type: " + value.showType()
				+ "\t" + "Name: " + key  + "\n"));
		if (result.toString().isEmpty()) {
			return "The directory " + this.getName() + " is empty.";
		} else {
			return result.toString();
		}
	}

	//////////////////
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Directory)) {
			return false;
		}

		Directory otherDir = (Directory) o;
		return (dirName.equals(otherDir.getName()) && children.equals(otherDir.children));
	}

}