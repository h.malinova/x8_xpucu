package file.system;

public interface SystemFiles {
	String getName();
	Directory getParent();
	void setParent(Directory newParent);
	int getSize();
	String showType();
}
