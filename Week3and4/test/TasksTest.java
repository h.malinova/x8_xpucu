import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Stack;

public class TasksTest {

	@Test
	public void testConvertDecimalNumberToBinary() {
		String a = "101";
		String b = "1100";
		String c = "10";
		assertEquals(a, Tasks.convertDecimalNumberToBinary(5));
		assertEquals(b, Tasks.convertDecimalNumberToBinary(12));
		assertEquals(c, Tasks.convertDecimalNumberToBinary(2));
		
	}

	@Test
	public void testTowersOfHanoi() {
		Stack<Integer> start = new Stack<Integer>();
		Stack<Integer> expexted = new Stack<Integer>();

		for (int i = 5; i > 0; i--) {
			start.push(i);
			expexted.push(i);
		}

		Stack<Integer> help = new Stack<Integer>();
		Stack<Integer> end = new Stack<Integer>();

		Tasks.towersOfHanoi(5, start, help, end);

		assertTrue(start.isEmpty());
		assertTrue(help.isEmpty());
		assertEquals(expexted, end);
	}

	@Test
	public void testFindWholeNumberInString() {
		String a = "70pos aus2 199";
		assertEquals("70", Tasks.findWholeNumberInString(a, 0));
		assertEquals("2", Tasks.findWholeNumberInString(a, 9));
		assertEquals("199", Tasks.findWholeNumberInString(a, 11));
	}

	@Test
	public void testCalculate() {
		assertEquals(11, Tasks.calculate(5, 6, '+'));
		assertEquals(15, Tasks.calculate(5, 3, '*'));
		assertEquals(7, Tasks.calculate(10, 3, '-'));
		assertEquals(2, Tasks.calculate(10, 5, '/'));
	}

	@Test
	public void testEvaluateExpression() {
		String expr = "(((20-5) / 5) * 3)";
		assertEquals(9, Tasks.evaluateExpression(expr));
	}

	// TODO Test for SpanStockProblem
	
	@Test
	public void findMinMissing_Should_ReturnMinMissingElement_When_ItIsBetween1toN() {
		int[] a = {3,2,5,10};
		int[] b = {3,8,7,6,2,1};
		assertEquals(1, Tasks.findMinMissing(a));
		assertEquals(4, Tasks.findMinMissing(b));
	}
	
	@Test
	public void findMinMissing_Should_ReturnMinMissingElement_When_ItIsN() {
		int[] a = {1,6,4,2,5,3};
		assertEquals(7, Tasks.findMinMissing(a));
	}
	
	@Test
	public void findMinMissingSorted() {
		int[] a = {1,1,2,2,2,4};
		int[] b = {1,2,4,4,5};
		int[] c = {1,7,12};
		assertEquals(3, Tasks.findMinMissingSorted(a));
		assertEquals(3, Tasks.findMinMissingSorted(b));
		assertEquals(2, Tasks.findMinMissingSorted(c));
	}

	@Test
	public void bracketCorrectnessTrue() {
		assertTrue(Tasks.bracketCorrectness("(20+3)* {19+23} 0wdw[]"));
		assertTrue(Tasks.bracketCorrectness("({[]})"));
		assertTrue(Tasks.bracketCorrectness("({})[]"));
		
	}

	@Test
	public void bracketCorrectnessFalse() {
		assertFalse(Tasks.bracketCorrectness("(20+3 - [12+2)]"));
		assertFalse(Tasks.bracketCorrectness("(15 + {12 + 6}"));
		assertFalse(Tasks.bracketCorrectness("[{(})]"));
		
	}

	@Test
	public void expandString() {
		assertEquals("ABAB", Tasks.expandString("2(AB)"));
		assertEquals("ABCCBCCABCCBCC", Tasks.expandString("2(A2(B2(C)))"));
		assertEquals("ABCDDDDDDDDDD", Tasks.expandString("ABC10(D))"));
		// h2(2(a)2(r))
	}

	@Test
	public void nearestBiggerNumber() {
		assertArrayEquals(new int[] {5, 12,12,-1,12}, Tasks.nearestBiggerNumber(new int[] {4,5,3,12,4}));
		assertArrayEquals(new int[] {-1,-1,-1}, Tasks.nearestBiggerNumber(new int[] {11, 11, 11}));
		assertArrayEquals(new int[] {3,4,5,-1}, Tasks.nearestBiggerNumber(new int[] {2,3,4,5}));
		assertArrayEquals(new int[] {33, 4, 33, -1}, Tasks.nearestBiggerNumber(new int[] {6, 2, 4, 33}));
	}

}
