import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
// import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class MergeFiles {
	static void fillFile(String filePath, int lines) throws IOException {
		File file = new File(filePath);
		long number;
		Writer writer = new OutputStreamWriter(new FileOutputStream(file));
		for (int i = 0; i < lines; i++) {
			Random r = new Random();
			number = ((r.nextInt(6001) * r.nextInt(153)) + r.nextInt(25000));
			writer.write(String.valueOf(number));
			writer.write(System.lineSeparator());
		}
		writer.close();
	}

	// 2 big files with numbers - sort and merge

	// split big file to 'x' smaller files
	// sort all of the small files
	// merge small sorted files into 1 big sorted file

	// inputFilePath: where the big file is;
	// newFilesFolderPath: where to store the small files
	// linesPerSplit: how many lines should a small file have
	// returns the amount of new files created
	static ArrayList<File> splitBigFileIntoSmallerFiles(String bigFilePath, double linesPerSplit) throws IOException {
		File bigFile = new File(bigFilePath);
		InputStream inputFileStream = new BufferedInputStream(new FileInputStream(bigFile));
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputFileStream));

		String line = reader.readLine();
		int linesWritten = 0;
		int count = 1; // number of new files

		ArrayList<File> splitFiles = new ArrayList<File>();

		// creates a newFile for every "linesPerSplit" lines
		while (line != null) {
			File newFile = new File(bigFile.getParent() + "\\" + "unsorted" + "_" + count + ".txt");
			Writer writer = new OutputStreamWriter(new FileOutputStream(newFile));

			// write "linesPerSplit" amount of lines in file: originalFile_1.split
			while (line != null && linesWritten < linesPerSplit) {
				writer.write(line);
				writer.write(System.lineSeparator());
				writer.flush();
				line = reader.readLine();
				linesWritten++;
			}
			splitFiles.add(newFile);
			writer.close();
			linesWritten = 0;
			count++;
		}

		reader.close();
		return splitFiles;
	}

	static File sortSmallFiles(File fileToSort, int position) throws IOException {
		File sortedFile = new File(fileToSort.getParent() + "\\" + "sorted_" + position + ".txt");
		Scanner scan = new Scanner(fileToSort);
		ArrayList<Integer> list = new ArrayList<Integer>();

		// zapisva vsichki intove fileToSort vuv list
		while (scan.hasNextInt()) {
			list.add(scan.nextInt());
		}
		Collections.sort(list);
		// da zapishe list v sortedFile
		Writer writer = new OutputStreamWriter(new FileOutputStream(sortedFile));
		for (int numberInList : list) {
			writer.write(String.valueOf(numberInList));
			writer.write(System.lineSeparator());
			writer.flush(); // ?
		}
		scan.close();
		writer.close();

		fileToSort.delete();
		return sortedFile;
		// sortedFile.renameTo(fileToSort);
	}

	static void mergeTwoFiles(File f1, File f2, File mergedFile) throws IOException {
		// File mergedFile = new File (endName);
		InputStream inputFileStream1 = new BufferedInputStream(new FileInputStream(f1));
		BufferedReader reader1 = new BufferedReader(new InputStreamReader(inputFileStream1));
		InputStream inputFileStream2 = new BufferedInputStream(new FileInputStream(f2));
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(inputFileStream2));

		// BufferedReader reader1 = new BufferedReader(new FileReader (f1));
		// BufferedReader reader2 = new BufferedReader(new FileReader (f2));
		Writer writer = new OutputStreamWriter(new FileOutputStream(mergedFile));

		String n1 = reader1.readLine();
		String n2 = reader2.readLine();

		while (n1 != null && n2 != null) {
			if (Integer.parseInt(n1) < Integer.parseInt(n2)) {
				writer.write(n1);
				writer.write(System.lineSeparator());
				n1 = reader1.readLine();
			} else {
				writer.write(n2);
				writer.write(System.lineSeparator());
				n2 = reader2.readLine();
			}
		}

		while (n1 != null) {
			writer.write(n1);
			writer.write(System.lineSeparator());
			n1 = reader1.readLine();
		}

		while (n2 != null) {
			writer.write(n2);
			writer.write(System.lineSeparator());
			n2 = reader2.readLine();
		}

		reader1.close();
		reader2.close();
		writer.close();
		f1.delete();
		f2.delete();
		// return mergedFile;
	}

	static void mergeAllSortedFiles(File bigFile, double linesPerSplit) throws IOException {
		ArrayList<File> smallFiles = splitBigFileIntoSmallerFiles(bigFile.getAbsolutePath(), linesPerSplit);
		ArrayList<File> sortedFiles = new ArrayList<File>();
		for (int i = 0; i < smallFiles.size(); i++) {
			sortedFiles.add(sortSmallFiles(smallFiles.get(i), i + 1));
		}
		String path = bigFile.getParent();
		bigFile.delete();
		
		String[] nameOfFile = ((bigFile.getName()).split("/.", 2));
		System.out.println((nameOfFile[0]));
		File bigMerged = new File(path + "\\" + nameOfFile[0]  + "merged.txt");
		
		//File bigMerged = new File(path + "\\" + "merged.txt");
		Writer write = new OutputStreamWriter(new FileOutputStream(bigMerged));
		write.close();
		File temp = new File(path + "temp");
		// File bigMerged = sortedFiles.get(0);
		for (int i = 0; i < sortedFiles.size(); i++) {
			mergeTwoFiles(sortedFiles.get(i), bigMerged, temp);
			temp.renameTo(bigMerged);
		}
	}
}
