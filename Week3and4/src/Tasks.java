import java.util.*;
import java.io.*;

public class Tasks {

	static public String convertDecimalNumberToBinary(int decimalNum) {
		Stack<String> stack = new Stack<String>();
		int remainder;
		while (decimalNum != 0) {
			remainder = decimalNum % 2;
			decimalNum /= 2;
			stack.push(String.valueOf(remainder));
		}
		StringBuilder binaryNum = new StringBuilder();
		while (!stack.isEmpty()) {
			binaryNum.append(stack.pop());
		}
		return binaryNum.toString();
	}

	/////////////////////////////////////////////////////////////////

	static void towersOfHanoi(int numOfDiscs, Stack<Integer> start, Stack<Integer> help, Stack<Integer> end) {
		if (numOfDiscs == 0)
			return;
		else {
			towersOfHanoi(numOfDiscs - 1, start, end, help); // ot start da otide do help chrez end
			end.push(start.pop());
			towersOfHanoi(numOfDiscs - 1, help, start, end);
		}
	}

	/////////////////////////////////////////////////////////////////

	static String findWholeNumberInString(String expr, int startPosition) {
		StringBuilder temp = new StringBuilder();
		char digit;
		while (startPosition < expr.length() && Character.isDigit(expr.charAt(startPosition))) {
			digit = expr.charAt(startPosition);
			temp.append(digit);
			startPosition++;
		}

		return temp.toString();
	}

	static int calculate(int first, int second, char operation) {
		switch (operation) {
		case '+':
			return (first + second);
		case '-':
			return (first - second);
		case '*':
			return (first * second);
		case '/':
			return (first / second);
		}
		return -1; // non-valid operation
	}

	static int evaluateExpression(String expr) {
		Stack<Character> symbols = new Stack<Character>();
		Stack<Integer> numbers = new Stack<Integer>();
		int size = expr.length();
		for (int i = 0; i < size; i++) {
			char c = expr.charAt(i);
			if (c == '(' || c == '*' || c == '/' || c == '+' || c == '-') {
				symbols.push(c);
			}
			if (Character.isDigit(c)) {
				if (Character.isDigit(expr.charAt(i + 1))) {
					String wholeNumber = findWholeNumberInString(expr, i);
					i += wholeNumber.length() - 1; // za da preskochi chisloto; -1, cuz i++ anyway
					numbers.push(Integer.parseInt(wholeNumber));
				} else {
					numbers.push(Character.getNumericValue(c));
				}
			}
			if (c == ')') {
				int second = numbers.pop();
				int first = numbers.pop();
				char operation = symbols.pop();
				int result = calculate(first, second, operation);
				numbers.push(result);
			}
		}
		return numbers.pop();
	}

	/////////////////////////////////////////////////////////////////

	static int[] stockSpanProblem(int[] input) {
		int days = input.length;
		int[] spanValues = new int[days];
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(0); // index of first element
		spanValues[0] = 1; // spanValue of day1 is always 1

		for (int i = 1; i < days; i++) {
			while (!stack.isEmpty() && input[stack.peek()] <= input[i]) {
				stack.pop();
			}
			if (stack.isEmpty()) {
				spanValues[i] = i + 1;
			} else {
				spanValues[i] = i - stack.peek();
			}
			stack.push(i);
		}
		return spanValues;
	}

	/////////////////////////////////////////////////////////////////

	// 1-n not sorted array, find min missing
	static int findMinMissing(int[] input) {
		int n = input.length;
		boolean[] occurrences = new boolean[n];
		for (int i = 0; i < n; i++) {
			if (input[i] <= n) {
				occurrences[input[i] - 1] = true;
			}
		}
		for (int i = 0; i < n; i++) {
			if (occurrences[i] == false)
				return i + 1;
		}
		return n + 1;
	}

	/////////////////////////////////////////////////////////////////

	static int findMinMissingSorted(int[] input) {
		int min = 1;
		for (int i = 0; i < input.length; i++) {
			if (input[i] == min)
				min++;
			else if (input[i] > min)
				return min;
		}
		return min;
	}

	// 1 2 2 2 4

	/////////////////////////////////////////////////////////////////

	static char opositeBracket(char symbol) {
		switch (symbol) {
		case ')':
			return '(';
		case '}':
			return '{';
		case ']':
			return '[';
		default:
			return '0';
		}
	}

	static boolean popFromStack(char symbol, Stack<Character> stack) {
		if (!stack.isEmpty() && stack.peek() == opositeBracket(symbol)) {
			stack.pop();
			return true;
		}
		return false;
	}

	// brackets correctness
	static boolean bracketCorrectness(String input) {
		int size = input.length();
		Stack<Character> brackets = new Stack<Character>();
		for (int i = 0; i < size; i++) {
			// open brackets
			char ch = input.charAt(i);
			if (ch == '(' || ch == '{' || ch == '[') {
				brackets.push(ch);
			}
			// close brackets
			if (ch == ')' || ch == '}' || ch == ']') {
				if (!popFromStack(ch, brackets)) {
					return false;
				}
			}
		}
		return brackets.isEmpty();
	}

	/////////////////////////////////////////////////////////////////

	static void fillStacks(String input, Stack<Character> symbols, Stack<Integer> numbers) {
		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			if (Character.isDigit(ch)) {
				if (Character.isDigit(input.charAt(i + 1))) {
					String wholeNumber = findWholeNumberInString(input, i);
					i += wholeNumber.length() - 1;
					numbers.push(Integer.parseInt(wholeNumber));
				} else {
					numbers.push(Character.getNumericValue(ch));
				}
			}
			if (Character.isLetter(ch) || ch == '(') {
				symbols.push(ch);
			}
		}
	}

	static StringBuilder returnWordFromStack(Stack<Character> input) {
		StringBuilder output = new StringBuilder();
		while (!input.isEmpty()) {
			output.append(input.pop());
		}
		return output;
	}

	static StringBuilder repeatWord(StringBuilder word, int times) {
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < times; i++) {
			output.append(word);
		}
		return output;
	}

	// AB3(DC)2(F) - expand to ABDCDCDCFF
	static String expandString(String input) {
		Stack<Character> symbols = new Stack<Character>();
		Stack<Integer> numbers = new Stack<Integer>();
		Stack<Character> help = new Stack<Character>();
		fillStacks(input, symbols, numbers);

		char currentSym;
		StringBuilder finalWord = new StringBuilder();

		while (!symbols.isEmpty()) {
			currentSym = symbols.pop();
			if (Character.isLetter(currentSym)) {
				help.push(currentSym);
			}
			if (currentSym == '(') {
				StringBuilder wordToRepeat = returnWordFromStack(help);
				wordToRepeat.append(finalWord);
				finalWord = repeatWord(wordToRepeat, numbers.pop());
			}
		}
		if (!help.isEmpty()) {
			StringBuilder helper = returnWordFromStack(help);
			finalWord.insert(0, helper);
		}
		return finalWord.toString();
	}

	/////////////////////////////////////////////////////////////////

	static boolean canThreeNumbersFormATriangle(int[] input) {
		Arrays.sort(input);
		for (int i = 0; i < input.length - 2; i++) {
			if (input[i] + input[i + 1] > input[i + 2])
				return true;
		}
		return false;
	}

	/////////////////////////////////////////////////////////////////

	static int[] nearestBiggerNumber(int[] input) {
		int size = input.length;
		int[] closest = new int[size];
		for (int i = 0; i < size; i++) {
			int current = input[i];
			int j = 1;
			boolean continueSearchingForNumber = true;
			while (continueSearchingForNumber && ((i + j < size) || (i - j > 0))) {
				if ((i + j < size) && current < input[i + j]) {
					closest[i] = input[i + j];
					continueSearchingForNumber = false;
				} else if ((i - j > 0) && current < input[i - j]) {
					closest[i] = input[i - j];
					continueSearchingForNumber = false;
				}
				j++;
			}
			if (continueSearchingForNumber) {
				closest[i] = -1;
			}
		}
		return closest;
	}

	/////////////////////////////////////////////////////////////////

	// abccddd -> ab2(c)3(d)
	// abcabcabc -> 3(abc) ??
	// hash Table
	// TODO
	static String compressFile(File file) {
		// TODO
		return "a";
	}

	/////////////////////////////////////////////////////////////////

}
